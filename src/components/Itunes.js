import React from 'react'


const Itunes = ({ Itunes }) => {
    return(
        <div>
        {Itunes.map((Itunes, index) => (
            <div className="container albumRow" key={index}>
                <div className="row">
                <div className="col-sm-1"> 
                <div className="row"> <img src={`${Itunes["im:image"][2].label}`} height="100" width="100" alt="album"></img></div>
                </div>
                <div className="col-sm-1">                
                <div className="row">&nbsp; &nbsp; &nbsp; &nbsp;{index +1}</div>
                </div>
                <div className="col-sm-5">
                <div className="row">Album: {Itunes["im:name"].label}</div>
                <div className="row">Artist: {Itunes["im:artist"].label}</div>
                <div className="row">Genre: {Itunes.category.attributes.label}</div>
                </div>
                <div className="col-sm-4">
                <div className="row">Price: {Itunes["im:price"].label}</div>
                <div className="row">Song Count: {Itunes['im:itemCount'].label}</div>
                <p className="row">Release Date: {new Date(`${Itunes["im:releaseDate"].label}`).toLocaleDateString()}</p>
                </div>        
                </div>
            </div>         
         ))}
        </div>
    )
};

export default Itunes
