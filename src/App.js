import React, { Component } from "react";
import Itunes from './components/Itunes';
import './App.css';

class App extends Component {

  state = {
    Itunes: [],
    Albums: [],
    showFilterModule: false,
    albumBox: "",
    artistBox: "",
    genreBox: "",
    searchBox: ""

  }
 
//sets two different states so the original list is never filtered
  componentDidMount() {
    fetch('https://itunes.apple.com/us/rss/topalbums/limit=100/json')
    .then(res => res.json())
    .then((data) => {
      this.setState({ Itunes: data.feed.entry })
      this.setState({ Albums: data.feed.entry })
    })
    .catch(console.log)
  }

clearFilters(){
  this.setState({ Albums: this.state.Itunes});
  this.setState({ genreBox: ""});
  this.setState({ artistBox: ""});
  this.setState({ albumBox: ""});
  this.setState({ searchBox: ""})
  this.setState({ showFilterModule: false});
}

showFilters(){
  this.setState({ showFilterModule: !this.state.showFilterModule})
}


//Updates advanced search textbox values and filters on AND
handleChange({ target }) {
  this.setState({
    [target.name]: target.value
  });

  let filteredAlbums = this.state.Itunes;

  if (target.name === "albumBox" || (this.state.albumBox && this.state.albumBox !== "")) {
    const selectedAlbum = target.name === "albumBox" ? target.value : this.state.albumBox;
    filteredAlbums = filteredAlbums.filter( x => {
    return x["im:name"].label.toLowerCase().includes(selectedAlbum.toLowerCase());
  })
  }

  if (target.name === "artistBox" || (this.state.artistBox && this.state.artistBox !== "")) {
    const selectedArtist = target.name === "artistBox" ? target.value : this.state.artistBox;

    filteredAlbums = filteredAlbums.filter( x => {
      return x["im:artist"].label.toLowerCase().includes(selectedArtist.toLowerCase());
    })
  }

  if (target.name === "genreBox" || (this.state.genreBox && this.state.genreBox !== "")) {
    const selectedGenre = target.name === "genreBox" ? target.value : this.state.genreBox;
    filteredAlbums = filteredAlbums.filter( x => {
    return x.category.attributes.label.toLowerCase().includes(selectedGenre.toLowerCase());
  })
  }

  this.setState({ Albums: filteredAlbums});

}

//Updates search bar textbox value and filters based on OR
 searchBar({target}) {
  this.setState({
    [target.name]: target.value
  });

  this.setState({ Albums: this.state.Itunes.filter(x => {
    return x["im:artist"].label.toLowerCase().includes(target.value.toLowerCase()) || x.category.attributes.label.toLowerCase().includes(target.value.toLowerCase()) || x["im:name"].label.toLowerCase().includes(target.value.toLowerCase());
    ;
    })
  })
 }

  render () {
    return (
      <div className="background">
      <center><h1 className="title">Itunes Top 100 Albums</h1></center>
      <div className="filterButtons">
      <input  type="text" name="searchBox" placeholder="Search here..."  value={ this.state.searchBox }  onChange={this.searchBar.bind(this)}></input>
      <button onClick={this.clearFilters.bind(this)}>Clear Search</button>
      <button onClick={this.showFilters.bind(this)}>Advanced Search</button>
      </div>      
      {this.state.showFilterModule ?   <div className="container">
      <div className="row">
      <div className="col-sm-5"> 
      <label>Search by Album:</label>
      </div>
      <div className="col-sm-5"> 
      <label>Search by Artist:</label>
      </div>
      <div className="col-sm-2"> 
      <label>Search Genre:</label>
      </div>
      </div>
      <div className="row bottomRow">
      <div className="col-sm-5"> 
      <input  type="text" name="albumBox" placeholder="Enter Album here..."  value={ this.state.albumBox }  onChange={this.handleChange.bind(this)}></input>
      </div>
      <div className="col-sm-5"> 
      <input type="text" name="artistBox" placeholder="Enter Artist here..."  value={ this.state.artistBox }  onChange={this.handleChange.bind(this)}></input>
      </div>
      <div className="col-sm-2"> 
      <input className ="topRightBox" type="text" name="genreBox" placeholder="Enter Genre here..."  value={ this.state.genreBox }  onChange={this.handleChange.bind(this)}></input>
      </div>     
      </div>
    </div>  : null}  
     <Itunes Itunes={this.state.Albums} />
     </div>
    );
  }
}

export default App;
